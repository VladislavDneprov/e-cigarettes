<?php
/**
 * Template Name: Контакты
 */
?>

<?php get_header(); ?>
<section class="page">
	<div class="container contact-page">
		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 info">
				<h2>Контактная информация</h2>
				<ul>
					<li>
						<b>Местонахождение:</b> Украина, Харьков, ул.Танкопия 95-А
					</li>
					<li>
						<b>Телефоны:</b> 
						<a href="tel:+38 068 404 14 33" title="Позвонить">+38 068 404 14 33,</a>
						<a href="tel:+38 093 729 22 44" title="Позвонить">+38 093 729 22 44</a>
					</li>
					<li>
						<b>E-mail:</b> <a href="mailto:e-liquds@gmail.com" title="Напишите нам">e-liquds@gmail.com</a>
					</li>
				</ul>
				<iframe width="100%" height="400px" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d41052.59873105967!2d36.233259561779775!3d49.97784503804027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1476775859557" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 cantact-form">
				<?php 
					echo do_shortcode('[contact-form-7 id="159" title="Написать нам"]');
				?>
			</div>

		</div>
	</div>
</section>
<?php get_footer(); ?>
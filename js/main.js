jQuery(function($){

	
	 
// Close anon function.

	$(document).ready(function(){

		$('.email').inputmask({
			mask: "*{2,}@${2,}.a{2,3}",
			definitions: {
		    	'*': {
		        	validator: "[0-9A-Za-z\._\-]",		       
		      	},
		      	'$': {
		      		validator: "[0-9A-Za-z_\-]",		
		      	}
		    }
		});
		$('.tel').inputmask('+38(999)999-9999');
		$('.department').inputmask({
			mask: "9{1,3} отделение",
			greedy: false,
		});
		/*$('a.ajax_add_to_cart').click(function(){
			setTimeout(function(){
				"use strict";
			// Define the PHP function to call from here
			 var data = {
			   'action': 'mode_theme_update_mini_cart'
			 };
			 $.post(
			   woocommerce_params.ajax_url, // The AJAX URL
			   data, // Send our PHP function
			   function(response){
			   	console.log(response);
			     $('.total').html(response); // Repopulate the specific element with the new content
			     return;
			   }
			 );
			}, 500);
			
		});*/

		$('.back').live('click', function(){
			$('.mini-cart-widget').addClass('hidden').hide();
			$(this).remove();
		});
		$('.open-list').live('click', function(){
			
			if($('.mini-cart-widget').hasClass('hidden')){
				$('.mini-cart-widget').removeClass('hidden').show(function(){
					$('body').append('<div class="back"></div>');
				});
			}
			else{
				
				$('.mini-cart-widget').addClass('hidden').hide();
			}
			
		});

		$('.top-slider').slick({
			slidesToShow: 1,
		    slidesToScroll: 1,
		    autoplay: false,
		    autoplaySpeed: 1500,
		    dots: true,
		    infinite: true,
		    arrows: true,
		   	nextArrow: '<i class="right fa fa-angle-right"></i>',
		   	prevArrow: '<i class="left fa fa-angle-left"></i>',
		    
		    responsive: [
			    {
			      breakpoint: 1199,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: true
			      }
			    },
			    {
			      breakpoint: 768,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        dots: false,
			        arrows: false
			      }
			    },
			    
	    	]
		});		
		
	});
});
<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage e-cigarettes
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/favicon.png" type="image/png">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php /* Все скрипты и стили теперь подключаются в functions.php */ ?>

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Courgette|Kaushan+Script" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
	<?php wp_head(); // необходимо для работы плагинов и функционала ?>
</head>
<body <?php body_class(); // все классы для body ?>>
	<header>
		<div class="container">
			<div class="contact-info row">
				<div class="logo col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<a href="<?php echo get_home_url(); ?>"><img src="<?= get_template_directory_uri().'/img/logo.png'?>"></a>					
				</div>
				<div class="col-lg-6 col-md-7 col-sm-8 col-xs-12">
					<div class="row">
						<div class="company-name col-lg-6 col-md-6 col-sm-6 col-xs-12 col-uxs-12">
							
								<span class="title other">Alpha&Omega</span>
								<span class="semi-title">E-liquids</span>
								<span>by</span>
								<span class="title">Jacob&Gregory Labs</span>
											

						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-uxs-12 text-center">							
							
							<span class="title"><i class="favicon fa fa-mobile"></i>+38 068 404 14 33</span>
							<ul>
								<li><span class="title">+38 093 729 22 44</span></li>								
							</ul>								
							
							<span>
								<a rel="nofollow" class="dotted" href="" data-toggle="modal" data-target="#modal-callback">
									<i class="fa fa-headphones"></i> 
									Заказать обратный звонок
								</a>
							</span>
							

						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-lg-offset-0 col-md-3 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-12 text-right ">
					<div class="mini-cart row">	
						
							<?//php woocommerce_mini_cart();?>
	                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("CartTop") ) : ?>
                        <?php endif; ?>								
						
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<?php wp_nav_menu(array('menu_class' => 'nav navbar-nav','container' => 'ul')); ?>
				
			</div>
		</nav>
	</header>


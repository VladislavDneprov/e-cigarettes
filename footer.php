<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage e-cigarettes
 */
?>
	<footer class="footer">
		<div class="container">
			<div class="row footer-content">
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 footer-link logo">
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?= get_template_directory_uri().'/img/logo.png'?>" alt="Alfa&OmegaE-liquids">
					</a>
				</div>

				<div class="col-xs-12 col-sm-2 col-md-3 col-lg-3 footer-link">
					<h5>Партнёрам</h5>
					<ul>
						<li><a href="#" title="Оплата">Для поставщиков</a></li>
						<li><a href="#" title="Оплата">Для опатовых покупателей</a></li>
						<li></li>
						<li><h5>Новости</h5></li>
						<li><a href="#" title="Оплата">Интересные статьи</a></li>
						<li><a href="#" title="Оплата">Карта сайта</a></li>
					</ul>
				</div>
				
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 footer-link">
					<h5>Сотрудничество</h5>
					<ul>
						<li><a href="#" title="Оплата">О компании</a></li>
						<li><a href="#" title="Оплата">Работав в компании, вакансии</a></li>
						<li><a href="#" title="Оплата">Регистрационные документы</a></li>
						<li><a href="#" title="Оплата">Пользовательское соглашение</a></li>
						<li><a href="#" title="Оплата">Обратная связь</a></li>
					</ul>
				</div>

				<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 contact-footer">
					<!--
					<h5><i class="fa fa-map-marker"></i>Посетите наш офис</h5>
					<ul>
						<li>ул.Сумская 51 Б-2</li>
						<li><a href="#" class="dotted" title="Показать на карте" data-toggle="modal" data-target="#modal-maps">Показать на карте</a></li>
					</ul> -->

					<h5><i class="fa fa-phone"></i>Позвоните по телефонам</h5>
					<ul>
						<li><a href="tel:+38 068 404 14 33" title="Позвонить">+38 068 404 14 33</a></li>
						<li><a href="tel:+38 093 729 22 44" title="Позвонить">+38 093 729 22 44</a></li>
					</ul>

					<h5><i class="fa fa-envelope"></i>Напишите на почту</h5>
					<ul>
						<li><a href="mailto:e-liquds@gmail.com" title="Показать на карте">e-liquds@gmail.com</a></li>
					</ul>

					<ul class="social">
						<li><a target="blank" href="#" title="Вконтакте"><i class="fa fa-vk"></i></a></li>
						<li><a target="blank" href="https://www.instagram.com/aoeliquids/" title="Инстаграм"><i class="fa fa-instagram"></i></a></li>
						<li><a target="blank" href="https://facebook.com/jacobandgreorylabs/" title="Инстаграм"><i class="fa fa-facebook"></i></a></li>
					</ul>
				</div>

			</div>

			<div class="row footer-bottom">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					&#169; www.e-liquids.com, все права защищены
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 developer">
					Сайт разработан и поддерживается компанией <a href="http://jaguar-team.com" title="Jaguar-team">Jaguar-team</a>
				</div>
			</div>
		</div>
		
		<!-- Modal Call Back -->
		<div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-labelledby="modalCallBackLabel">
		  	<div class="modal-dialog" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Обратный звонок</h4>
		      		</div>
		      		<!--[contact-form-7 id="157" title="Обратный звонок"]-->
			        	<?php echo do_shortcode('[contact-form-7 id="34" title="Заказать обратный звонок"]');?>
			    	
		    	</div>
		  </div>
		</div>

		<!-- Modal Success -->
		<div class="modal center fade" id="success" tabindex="-1" role="dialog" aria-labelledby="modalCallBackLabel">
		  	<div class="modal-dialog" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		      		</div>
		      		<div class="modal-body">
		      			<div class="alert alert-success">
							<strong>Ваше сообщение успешно отправлена!</strong>
						</div>

		      		</div>
			    	
		    	</div>
		  </div>
		</div>
		
	</footer>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>
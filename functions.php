<?php
/**
 */

add_theme_support('title-tag');

/** register nav-menu **/
register_nav_menus(array(
	'top' => 'Верхнее',
	'bottom' => 'Внизу'
));

add_theme_support('post-thumbnails');

/** add js **/
add_action('wp_footer', 'add_scripts');

if (!function_exists('add_scripts')) {
	function add_scripts() {
	    if(is_admin()) return false;

	    wp_enqueue_script('jquery'); 
	    wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js','','',true);
	    wp_enqueue_script('slick', get_template_directory_uri().'/js/slick/slick.min.js');
	    wp_enqueue_script('masks', get_template_directory_uri().'/js/jquery.maskedinput.js');
	    wp_enqueue_script('inputmask_jq', get_template_directory_uri().'/js/inputmask/jquey.inputmask.bundle.min.js');
	   
	    wp_enqueue_script('main', get_template_directory_uri().'/js/main.js');
	    
	}
}

/** add css style **/
add_action('wp_print_styles', 'add_styles');
if (!function_exists('add_styles')) {
	function add_styles() {
	    if(is_admin()) return false;
	    
	    wp_enqueue_style( 'bs', get_template_directory_uri().'/css/bootstrap.min.css' );
	    wp_enqueue_style( 'awesome', get_template_directory_uri().'/css/font-awesome.min.css' );
	    wp_enqueue_style( 'slick', get_template_directory_uri().'/js/slick/slick.css' );
	    wp_enqueue_style( 'slick-theme', get_template_directory_uri().'/js/slick/slick-theme.css' );
	   
		wp_enqueue_style( 'main', get_template_directory_uri().'/style.css' );

		wp_enqueue_script( 'ajax', get_template_directory_uri().'/js/ajax.js' );
		$vars = array( 'ajax_url' => admin_url( 'admin-ajax.php' ) );
		wp_localize_script( 'ajax', 'WC_VARIATION_ADD_TO_CART', $vars );
	}
}

/** register widgets **/
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'CartTop',
		'before_widget' => '',
		'after_widget' => ''
	));
}
/*function mode_theme_update_mini_cart() {
  echo wc_get_template( 'cart/mini-cart.php' );
  die();
}
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
/** woocommerce **/
add_action( 'after_setup_theme', 'woocommerce_support' );

function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {

    $currencies['UAH'] = __( 'Українська гривня', 'woocommerce' );

    return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
         case 'UAH': $currency_symbol = 'грн.'; break;
     }

     return $currency_symbol;
}

add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );
add_filter( 'woocommerce_checkout_fields' , 'custom_override_account_fields' );


function custom_override_account_fields($fields) {


	// unset($fields['account']['account_password']);

	return $fields;
}

function custom_override_billing_fields($fields) {


	/** remove **/
	unset($fields['billing_first_name']);
	unset($fields['billing_last_name']);
	unset($fields['billing_address_1']);
	unset($fields['billing_address_2']);
	unset($fields['billing_email']);
	unset($fields['billing_phone']);
	unset($fields['billing_country']);
	unset($fields['billing_company']);
	unset($fields['billing_state']);
	unset($fields['billing_city']);
	unset($fields['billing_postcode']);

	/** add new **/

	$fields['billing_full_name'] = array(
		'type' 			=> 'text',
		'label' 		=> 'Фамилия, имя, отчество',
		'clear' 		=> true,
		'placeholder' 	=> 'ФИО',
		'required' 		=> true
	);

	$fields['billing_phone'] = array(
		'type' 			=> 'text',
		'label' 		=> 'Контактный номер телефона',
		'clear' 		=> true,
		'placeholder' 	=> 'Номер телефона',
		'input_class'	=> ['tel'],
		'required' 		=> true
	);

	$fields['billing_city'] = array(
		'type' 			=> 'text',
		'label' 		=> 'Город',
		'placeholder' 	=> 'Введите город',
		'required' 		=> true,
		'clear' 		=> false
	);

	$fields['billing_address_1'] = array(
		'type' 			=> 'text',
		'label' 		=> 'Отделение Новой Почты',
		'placeholder' 	=> 'Пример: № 4',
		'required' 		=> true,
		'input_class'	=> ['department'],
		'clear' 		=> false
	);

	$fields['billing_email'] = array(
		'type' 			=> 'text',
		'label' 		=> 'E-mail',
		'input_class'	=> ['email'],
		'clear' 		=> true,
		'placeholder' 	=> 'Ваш действующий электронный адрес',
		'required' 		=> false
	);


	return $fields;
}


add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '
    	<p><strong>Полное имя:</strong> ' . get_post_meta( $order->id, '_billing_full_name', true ) . '</p>'
    ;
}

/** user functions **/
function crop_string($string, $count, $add_symbol = '')
{  
   return substr($string, 0, $count).((strlen($string) > $count) ? $add_symbol : '');
}






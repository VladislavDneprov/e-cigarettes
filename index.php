<?php
/**
 * Главная страница (index.php)
 * Template Name: e-cigarettes
 * @package WordPress
 * @subpackage e-cigarettes
 */
get_header(); // подключаем header.php ?>

<section class="home-abous-us">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                Представляем вашему вниманию линейку жидкостей для электронных сигарет премиум класса Alpha&Omega E-liquids, разработанной одной из ведущих лабораторий Jacob&Gregory labs. Линейка жидкостей Alpha&Omega E-liquids была создана отчаянными фанатами вэйпинга, которые бредили идеей создания идеальных незабываемых вкусов для вэйп сообщества по легкодоступным ценам. Осмелимся вам сообщить, что им это удалось. В линейку Alpha&Omega E-liquids входит не так много вкусов по той причине, что мы ставим на первое место качество! Мы не стремимся производить как можно больше разнообразных вкусов, чтобы заполнить рынок с целью наживы. Нашей целью является создание продуктов от которых вы сможете получать истинное наслаждение. Перед тем, как утвердить и добавить в нашу линейку новый вкус, мы раздаем образцы этого вкуса самым отъявленным представителям вэйп сообщества, для того чтобы узнать их мнение. Мы очень придирчиво и с большим вниманием оцениваем все замечания и только когда все идеально, новый вкус может пополнить ряды нашей линейки, и это становится знаменательным событием, потому что каждый раз мы дарим вам шедевр!
            </div>
        </div>
    </div>
</section>

<section class="top-slider">  	
  	
    	<div class="item">
      		<img src="<?= get_template_directory_uri().'/img/slider-home/1.jpg' ?>" alt="..." />
            <!--
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        Hello world
                    </div>
                </div>
            </div>
            -->
    	</div>
    	<div class="item">
      		<img src="<?= get_template_directory_uri().'/img/slider-home/2.jpg' ?>" alt="..." />
            <div class="carousel-caption">
            </div>
    	</div>
  	  	
</section>


<section class="featuder-product">
	<div class="container">
		<?php get_template_part('parts/product_list'); ?>
	</div>
</section>


<?php get_footer(); // подключаем footer.php ?>
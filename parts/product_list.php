<?php
	# aoeliquids ,alfaomegaliquids
	//wp_get_attachment_image($product_data->get_image_id());
    global $woocommerce;
	$args     = array( 'post_type' => 'product');
	$products = get_posts( $args );
?>
<section class="product-list">
	<div class="container">
		<div class="row">
			<?php foreach ($products as $key => $product): ?>
					<?php
						$product_data = wc_get_product($product->ID);
						$slug_cat = get_the_terms($product->ID, 'product_cat')[0]->slug;
					?>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 product-item">
						<a title="<?= $product->post_title; ?>" href="<?= $product_data->get_permalink(); ?>" class="<?= ($slug_cat == 'left' ? 'left' : 'right') ?>">
							<div class="img"><?= wp_get_attachment_image($product_data->get_image_id(), 'full'); ?></div>
		                	<div class="title"><span><?= $product->post_title; ?></span></div>
		                	<div class="desc"><?= $product->post_excerpt; ?></div>
		                </a>
		            </div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
